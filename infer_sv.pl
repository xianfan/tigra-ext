#!/usr/bin/perl -w
#
#Auther: Zechen Chong, Xian Fan, Ken Chen
#
#use strict;
use warnings;
use Getopt::Std;
#require sam;
#require fa;
my $ignore_seq_context = 1;
my %opts = ('o'=>'out.vcf', 's'=>10, 'l'=>20);
getopts('o:s:l:', \%opts);
die("Version: infer_sv 1.0
		Usage: perl infer_sv.pl -o [output] -s [INS gap length] -l [micro-insertion length] [sam_file]
		Options:
		-o	output vcf file [out.vcf]
		-s	insertion gap limit [$opts{s}]
		-l	minimum length of micro-insertion for checking if it is templated [$opts{l}]
		\n") unless ($#ARGV >= 0);

my $output = $opts{o};

# analyze alignment
open fh_, "<$ARGV[0]" or die $!;
my @lines = ();
while(<fh_>){
	push @lines, $_;
}
close fh_;

my $microhomology_threshold = 0;
# check if the insertion is nontemplated or templated, by looking at if length of insertion greater than this
my $check_insertion_threshold = $opts{l};
my $insertion_threshold = 0;
# the two breakpoints indicating an INS with the length of gap
my $ins_gap = $opts{s};
my $search_length = 500;
# for checking insertion, in bwa aln
my $seed_len = 5;
@lines = grep {!/^@/} @lines;

my @slines = ();
my @dlines = ();

##INFO=<ID=MATEID,Number=1,Type=String,Description="Breakend mate">
my $id = 0;
open out_fh, ">$output" or die $!;
print out_fh qq(##fileformat=VCFv4.1
##phasing=none
##INDIVIDUAL=TRUTH
##SAMPLE=<ID=TRUTH,Individual="TRUTH",Description="bamsurgeon spike-in">
##INFO=<ID=CIPOS,Number=2,Type=Integer,Description="Confidence interval around POS for imprecise variants">
##INFO=<ID=PRECISE,Number=0,Type=Flag,Description="Precise structural variation">
##INFO=<ID=SVTYPE,Number=1,Type=String,Description="Type of structural variant">
##INFO=<ID=SVLEN,Number=.,Type=Integer,Description="Difference in length between REF and ALT alleles">
##INFO=<ID=SOMATIC,Number=0,Type=Flag,Description="Somatic mutation in primary">
##INFO=<ID=CHR2,Number=1,Type=String,Description="Chromosome for END coordinate in case of a translocation">
##INFO=<ID=UNKNOWN_LEN,Number=0,Type=Flag,Description="Unknown the length of SV">
##INFO=<ID=CTG,Number=1,Type=String,Description="The contig name that contributes to this call">
##ALT=<ID=INV,Description="Inversion">
##ALT=<ID=DUP,Description="Duplication">
##ALT=<ID=DEL,Description="Deletion">
##ALT=<ID=INS,Description="Insertion">
##ALT=<ID=CTX,Description="Translocation">
##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	SPIKEIN
	       );
my $pre = "";
my @results = ();
for (my $i = 0; $i < @lines; $i++) {
	my @e = split /\s+/, $lines[$i];
	next if $e[1] == 4 or $e[5]!~/[SH]/;
	if ($e[5] =~ /^(\d+)[SH].+M(\d+)[SH]$/) { # avoid those that have both header and tail not aligned well
# actually needs to be trimmed in case of misassembly; TODO
		next if ($1>5 and $2>5);
	}
	if ($e[5] =~ /^(\d+)M.+?(\d+)M$/) { # avoid those that have both header and tail aligned well
		next if ($1>10 and $2>10);
	}
	if ($e[0] ne $pre) {
		if ($pre ne "") {
			if (@dlines >= 2) {
#				push @results, &parse_bp1_pairing(\@dlines, scalar(@dlines));}
# add those dlines to slines if no results in the previous read
				push @results, &parse_bp1_pairing(\@dlines, scalar(@dlines));
			}
			my $l = $dlines[0];
			my @ee = split /\s+/, $l;
			my $s = 0;
			while ($ee[5] =~ /(\d+)[SH]/g) {
				$s = $1 if $1 > $s;
			}
			push @slines, @dlines if $s > 20;
			@dlines = ();
		} 
		$pre = $e[0];
		push @dlines, $lines[$i];
	} else {
		push @dlines, $lines[$i];
	}
}

# in some cases, there are three alignments, in which ALN1 can be paired with ALN2 and ALN3 equally well. Need to take this into account.
if (@dlines >= 2) {
#	push @results, &parse_bp1_pairing(\@dlines, scalar(@dlines));
# same change as before
	push @results, &parse_bp1_pairing(\@dlines, scalar(@dlines));
}

# push all into slines anyway in case some slines have pairs in dlines for TRA, but actually an INS, computationally more complex in this case
my $l = $dlines[0];
my @ee = split /\s+/, $l;
my $s = 0;
while ($ee[5] =~ /(\d+)[SH]/g) {
	$s = $1 if $1 > $s;
}
push @slines, @dlines if $s > 20;

push @results, &parse_ins(\@slines);
#print @slines;

@results = sort {$a->[0] cmp $b->[0] or $a->[1] <=> $b->[1]} @results;
my $prepos = 0;
for (my $i = 0; $i < @results; $i++) {
	print out_fh join("\t", @{$results[$i]}), "\n";
}
close out_fh;

1;

sub parse_ins {
	my $l = shift;
	my @lines = @$l;
	my @bps = ();
	my @ret = ();
	for (my $i = 0; $i < @lines; $i++) {
		my @e = split /\s+/, $lines[$i];
		my ($m1, $s1) = (0, 0);
		while ($e[5] =~ /(\d+)[SH]/g) {
			$s1 = $1 if $1 > $s1;
		}
		while ($e[5] =~ /(\d+)[MD]/g) {
			$m1 = &sum($m1, $1);
		}
# avoid those matched well
		next if($e[5] !~ /^\d+[SH].+\d+M$/ && $e[5] !~ /^\d+M.+\d+[SH]$/);
		if ($e[5] =~ /\d+[M].*?$s1[SH]/) {
			$e[3] = $e[3]+$m1-1;
		}
		push @bps, [ ($e[2],$e[3],$e[1], $e[4],$e[5], $e[0]) ];	
	}
	@bps = sort {$a->[0] cmp $b->[0] or $a->[1] <=> $b->[1]} @bps;
	my @pre = ();
	my @cur = ();
	foreach (@bps) {
		@cur = @$_;
		if (@pre) {
			if ($cur[0] eq $pre[0] and $cur[1]-$pre[1]<$ins_gap) { #TODO magic number
				my ($m1, $s1, $m2, $s2) = (0, 0, 0, 0);
				while ($pre[4] =~ /(\d+)[SH]/g) {
					$s1 = &sum($s1, $1);
				}
				while ($pre[4] =~ /(\d+)[MD]/g) {
					$m1 = &sum($m1, $1);
				}
				while ($cur[4] =~ /(\d+)[SH]/g) {
					$s2 = &sum($s2, $1);
				}
				while ($cur[4] =~ /(\d+)[MD]/g) {
					$m2 = &sum($m2, $1);
				}
# need to add one more constraint: pre pos < cur pos for the first arg, and pos > cur for the second
				if (($pre[4]=~/\d+[M].*?\d+[SH]$/ and $cur[4]=~/^\d+[SH].*?\d+[M]/) or ($pre[4]=~/^\d+[SH].*?\d+[M]/ and $cur[4]=~/\d+[M].*?\d+[SH]$/ and $pre[1] + $m1 > $cur[1] - $m2)){
					push @ret, [ ($cur[0], $pre[1], "N", ".", "<INS>", ($pre[3]+$cur[3])/2, "PASS", "PRECISE;CIPOS=-10,10;CIEND=-10,10;SOMATIC;SVTYPE=INS;CHR2=$cur[0];END=$cur[1];UNKNOWN_LEN;SVLEN=".($cur[1]-$pre[1]).";CTG=$cur[-1]", "GT", "./.") ];
				}
			} 
			@pre = @cur;
		} else {
			@pre = @cur;
		}
	}
	return @ret;
}

sub sum {
	my ($s1, $l1) = @_;
	return $s1 + $l1;
}

sub max {
	my ($s1, $l1) = @_;
	return $s1 > $l1 ? $s1 : $l1;
}

# In case there are more than 2 alignments for the same read, make all possible pairs. 
sub parse_bp1_pairing {
	my ($lines, $num) = @_;
	my @lines = @$lines;
	my @results;
	foreach my $i (0 .. $num - 2){
		foreach my $j ($i + 1 .. $num - 1){
			push @results, &parse_bp1($lines[$i], $lines[$j]);
		}
	}
	return @results;
}

sub parse_bp1 { 
	my $line1 = shift;
	my $line2 = shift;
	my @ret = ();
	my ($m1, $s1, $m2, $s2) = (0, 0, 0, 0);
	my @e1 = split /\s+/, $line1;
	my @e2 = split /\s+/, $line2;
	if ($e1[5] =~ /[SMHD]/ and $e2[5] =~ /[SMHD]/) {
		while ($e1[5] =~ /(\d+)[SH]/g) {
			$s1 = &sum($s1, $1);
#					$s1 = $1; if $1 > $s1; 
		}
		while ($e1[5] =~ /(\d+)[MD]/g) {
			$m1 = &sum($m1, $1);
#					$m1 = $1 if $1 > $m1;
		}

		while ($e2[5] =~ /(\d+)[SH]/g) {
			$s2 = &sum($s2, $1);
#					$s2 = $1 if $1 > $s2;
		}
		while ($e2[5] =~ /(\d+)[MD]/g) {
			$m2 = &sum($m2, $1);
#					$m2 = $1 if $1 > $m2;
		}
	}
	my ($pos1, $pos2) = (0, 0);
	if ($e1[5] =~ /^$s1[SH].*?[M]/) {
		$pos1 = $e1[3];
	} else {
		$pos1 = $e1[3]+$m1-1;
	}
	if ($e2[5] =~ /^$s2[SH].*?[M]/) {
		$pos2 = $e2[3];
	} else {
		$pos2 = $e2[3]+$m2-1;
	}
	if (abs($m1-$s2)<=500 and abs($m2-$s1)<=500) {
		my $micro_str = "NA";
		my $insertion_str = "NA";
		my $template_str = "NA";
		my $non_template_str = "NA";
		if(!$ignore_seq_context){
			if($m1 - $s2 > $microhomology_threshold && $m2 - $s1 > $microhomology_threshold){
# check microhomology
				my $diff = ($m1 - $s2 > $m2 - $s1 ? $m1 - $s2 : $m2 - $s1);
				$micro_str = &check_microhomology($line1, $line2, $diff);
# new rule: always change the longer M one (make it shorter) no matter if any micro_str
				if($m1 > $m2){
					if($e1[5] =~ /^\d+[SH]/){
# clip at the head
						$pos1 += $diff;
					}
					elsif($e1[5] =~ /\d+[SH]$/){
						$pos1 -= $diff;
					}
				}
				else{
					if($e2[5] =~ /^\d+[SH]/){
						$pos2 += $diff;
					}
					elsif($e2[5] =~ /\d+[SH]$/){
						$pos2 -= $diff;
					}
				}

			}
			elsif($s2 - $m1 > $insertion_threshold && $s1 - $m2 > $insertion_threshold){
# TODO extract insertion str from the contig
				my $str;
				my $flag;
				if(length($e1[9]) > length($e2[9])){
					$str = $e1[9];
					$flag = $e1[1];
				}
				else{
					$str = $e2[9];
					$flag = $e2[1];
				}
				$insertion_str = &get_insertion($e1[5], $e2[5], $e1[1], $e2[1], $str, $flag);
				$non_template_str = $insertion_str;
# check if template or nontemplate by bwa aln
				($template_str, $non_template_str) = &check_local_insertion($insertion_str, $e1[2], $e2[2], $pos1, $pos2) if(length($insertion_str) > $check_insertion_threshold);
			}
		}
# rule: always change pos2 if micro_str

		if ($e1[2] ne $e2[2]) { # trans
			my $id2 = $id+1;
			push @ret, [ ($e1[2], $pos1, "N", ".", "<CTX>", ($e1[4]+$e2[4])/2, "PASS", "PRECISE;CIPOS=-10,10;CIEND=-10,10;SOMATIC;SVTYPE=CTX;CHR2=$e2[2];END=$pos2;SVLEN=0;MICRO=$micro_str;TEMPLATE=$template_str;NONTEMPLATE=$non_template_str;CTG=$e1[0]", "GT", "./.")];
			$id = $id2+1;
		} else {
			if ((($e1[1] & 16) and !($e2[1] & 16)) or (!($e1[1] & 16)) and ($e2[1] & 16)) { # inv
				if(abs($pos2 - $pos1) < 10) { # TODO magic number controling inv size
				} 
				else {
					if ($pos1 < $pos2) {
						push @ret, [ ($e2[2], $pos1, "N", ".", "<INV>", ($e1[4]+$e2[4])/2, "PASS", "PRECISE;CIPOS=-10,10;CIEND=-10,10;SOMATIC;SVTYPE=INV;CHR2=$e2[2];END=$pos2;SVLEN=".abs($pos2-$pos1+1).";MICRO=$micro_str;TEMPLATE=$template_str;NONTEMPLATE=$non_template_str;CTG=$e2[0]", "GT", "./.") ] ;
					} else {
						push @ret, [ ($e2[2], $pos2, "N", ".", "<INV>", ($e1[4]+$e2[4])/2, "PASS", "PRECISE;CIPOS=-10,10;CIEND=-10,10;SOMATIC;SVTYPE=INV;CHR2=$e2[2];END=$pos1;SVLEN=".abs($pos2-$pos1+1).";MICRO=$micro_str;TEMPLATE=$template_str;NONTEMPLATE=$non_template_str;CTG=$e2[0]", "GT", "./.") ];
					}
				}
			} else {
				if ($pos1 < $pos2) {
					if ($e1[5]=~/^\d+[M].*?\d+[SH]/ && $e2[5] =~ /^\d+[SH].*?\d+[M]/) { # del
						if(abs($pos2 - $pos1) > 10) { # TODO magic number controling del size
							push @ret, [ ($e2[2], $pos1, "N", ".", "<DEL>", ($e1[4]+$e2[4])/2, "PASS", "PRECISE;CIPOS=-10,10;CIEND=-10,10;SOMATIC;SVTYPE=DEL;CHR2=$e2[2];END=$pos2;SVLEN=".($pos1-$pos2).";MICRO=$micro_str;TEMPLATE=$template_str;NONTEMPLATE=$non_template_str;CTG=$e2[0]", "GT", "./.")] ;
						}
					} else { # dup
						push @ret, [ ($e2[2], $pos1, "N", ".", "<DUP>", ($e1[4]+$e2[4])/2, "PASS", "PRECISE;CIPOS=-10,10;CIEND=-10,10;SOMATIC;SVTYPE=DUP;CHR2=$e2[2];END=$pos2;SVLEN=".($pos2-$pos1).";MICRO=$micro_str;TEMPLATE=$template_str;NONTEMPLATE=$non_template_str;CTG=$e2[0]", "GT", "./." )];
					}
				} else {
					if ($e2[5]=~/^\d+[M].*?\d+[SH]/ && $e1[5] =~ /^\d+[SH].*?\d+[M]/) { # del
						if(abs($pos2 - $pos1) > 10) { # TODO magic number controling del size
							push @ret, [ ($e2[2], $pos2, "N", ".", "<DEL>", ($e1[4]+$e2[4])/2, "PASS", "PRECISE;CIPOS=-10,10;CIEND=-10,10;SOMATIC;SVTYPE=DEL;CHR2=$e2[2];END=$pos1;SVLEN=".($pos2-$pos1).";MICRO=$micro_str;TEMPLATE=$template_str;NONTEMPLATE=$non_template_str;CTG=$e2[0]", "GT", "./.") ];
						}
					} else { # dup
						push @ret, [ ($e2[2], $pos2, "N", ".", "<DUP>", ($e1[4]+$e2[4])/2, "PASS", "PRECISE;CIPOS=-10,10;CIEND=-10,10;SOMATIC;SVTYPE=DUP;CHR2=$e2[2];END=$pos1;SVLEN=".($pos1-$pos2).";MICRO=$micro_str;TEMPLATE=$template_str;NONTEMPLATE=$non_template_str;CTG=$e2[0]", "GT", "./.") ];
					}
				}
			}
		}
	}
	return @ret;
}

# check if the string appear in the affinity of breakpoints ( +- 100bp)
sub check_local_insertion{
	my ($str, $chr1, $chr2, $pos1, $pos2) = @_;
	my ($start, $end) = ($pos1 - $search_length, $pos1 + $search_length);
	my $str1 = &seq_from_ref($chr1, $start, $end);
	($start, $end) = ($pos2 - $search_length, $pos2 + $search_length);
	my $str2 = &seq_from_ref($chr2, $start, $end);
	my $len = length($str);
	my $h1 = &make_hash($str1, $len);
	my $h2 = &make_hash($str2, $len);
	if(defined $h1->{$str} || defined $h2->{$str}){
		return ($str, "NA");
	}
	return ("NA", $str);
}

# make hash table from string, with length specified for the keys
sub make_hash{
	my ($str, $len) = @_;
	my $h;
	my $str_rev = &reverse_comp($str);
	foreach my $i (0 .. length($str) - $len){
		my $str_ = substr $str, $i, $len;
		my $str_2 = substr $str_rev, $i, $len;
		$h->{$str_} = 1;
		$h->{$str_2} = 1;
	}
	return $h;
}

# TODO extract insertion str from the contig sam
sub get_insertion{
	my ($cigar1, $cigar2, $flag1, $flag2, $seq, $flag) = @_;
	my $len = length($seq);
# get the position lastly matched (before soft-clipped) on the read
	my $pos1 = &get_pos_on_string($cigar1, $len, $flag1);
	my $pos2 = &get_pos_on_string($cigar2, $len, $flag2);
	return &extract_seq($seq, $pos1, $pos2, $flag);
}

# extract the sequence according to pos1 and pos2, with pos1 < ? > pos2
sub extract_seq{
	my ($seq, $pos1, $pos2, $flag) = @_;
	my $pos = $pos1 < $pos2 ? $pos1 : $pos2;
	return substr($seq, $pos, abs($pos2 - $pos1)) if(!($flag & 0x0010));
	return substr($seq, length($seq) - $pos - abs($pos2 - $pos1), abs($pos2 - $pos1));
}

# parse cigar string and return the position of the lastly matched base before soft-clipped one
sub get_pos_on_string{
	my ($cigar, $len, $flag) = @_;
	my $tag = 0;
	if($cigar =~ /^\d+M/){
		$tag = 1;
	}
	my $match = 0;
	while($cigar ne ""){
		if($cigar =~ /^(\d+)[MI]/){
			$match += $1;
		}
		$cigar =~ s/^(\d+)\S//;
	}
	if($tag == 1){
		if($flag & 0x0010){
# reverse
			return $len - $match;
		}
		else{
			return $match;
		}
	}
	else{
		if($flag & 0x0010){
# reverse
			return $match;
		}
		else{
			return $len - $match;
		}
	}
}

# given a string, check if it's templated insertion
sub check_insertion{
	my $str = shift;
# make a fastq file 
	my $fq_file = fa::make_fq($str);
	my $bwa_aln_file = &run_bwa_aln($fq_file);
# check if any alignment in bwa_aln_file 
	if(sam::check_sam($bwa_aln_file) == 1){
		return ($str, "NA");
	}
	else{
		return ("NA", $str);
	}
}

# run bwa aln
sub run_bwa_aln{
	my $fq = shift;
	my $sai = $fq . ".sai";
	my $sam = $fq . ".sam";
	`$bwa aln -l $seed_len -t 8 $ref $fq > $sai`;
	`$bwa samse -n 1 $ref $sai $fq > $sam`;
	return $sam;
}


# given two alignments, and the maximum length of putative microhomology, return the string of microhomology if any
sub check_microhomology{
	my ($line1, $line2, $length_diff) = @_;
	my @a1 = split(/\s+/, $line1);
	my @a2 = split(/\s+/, $line2);
	my ($chr1, $chr2) = ($a1[2], $a2[2]);
	my ($pos1, $pos2) = ($a1[3], $a2[3]);
	my ($str1, $str2);
	$str1 = &get_putative_microhomology($a1[5], $chr1, $pos1, $length_diff);
	$str2 = &get_putative_microhomology($a2[5], $chr2, $pos2, $length_diff);
	return &compare_str($str1, $str2);
}

# this should be an exact match between two strings (or their substring), including reverted complement
sub compare_str{
	my ($str1, $str2) = @_;
	return "NA" if(length($str1) == 0 || length($str2) == 0);
# using suffix tree to find the longest common substring
# now simply assume the whole matches to the whole
	my $str2_ = &reverse_comp($str2);
	my $str1_ = &reverse_comp($str1);
	if($str1 eq $str2){
		return $str1;
	}
	elsif($str1 eq $str2_){
		return $str1;
	}
	elsif($str2 eq $str1_){
		return $str2;
	}
	return "NA";
}

sub reverse_comp{
	my ($str) = shift;
	my $str_rev = scalar reverse("$str");
	$str_rev =~ tr/ACGT/TGCA/;
	return $str_rev;
}

# get the putative microhomology string from one alignment
sub get_putative_microhomology{
	my ($cigar, $chr, $pos, $length_diff) = @_;
	my $str = "";
	if($cigar =~ /^(\d+)[SH].+M$/){
		$str = &extract_str($chr, $pos, $length_diff, 1);
	}
	elsif($cigar =~ /^(\d+)M.+[SH]$/){
		my $length = &analyze_cigar($cigar);
		$str = &extract_str($chr, $pos + $length, $length_diff, 2);
	}
	return $str;
}

# return the length of string that takes place on the reference
sub analyze_cigar{
	my ($cigar) = shift;
	my $len = 0;
	while($cigar ne ""){
		if($cigar =~ /^(\d+)(\S)/){
			my ($num, $str) = ($1, $2);
			$cigar =~ s/^$num$str//;
			if($str eq "M" || $str eq "D"){
				$len += $num;
			}
		}
	}
	return $len;
}


# extract the substr from reference given both soft-clipped at first and at last
sub extract_str{
	my ($chr, $pos, $length_diff, $tag) = @_;
	my ($start, $end);
	if($tag == 1){
# soft-clipped at first
		$end = $pos + $length_diff - 1;
		$start = $pos;
	}
	elsif($tag == 2){
# soft-clipped at last
		$start = $pos - $length_diff;
		$end = $pos - 1;
	}
	return &seq_from_ref($chr, $start, $end);
}

sub seq_from_ref{
# extract seq from reference and return only the sequence
	my ($chr, $start, $end) = @_;
	$start = 0 if($start < 0);
	my $out = `samtools faidx $ref $chr:$start-$end`;
	my @tmp = split(/\n/, $out);
	my $str = "";
	foreach (@tmp){
		chomp;
		next if($_ =~ /^>/ || $_ eq "");
		$str .= $_;
	}
	return $str;
}


